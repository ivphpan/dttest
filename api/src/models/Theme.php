<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%theme}}".
 *
 * @property int $id
 * @property int|null $categoryId Категория
 * @property int|null $levelId Уровень
 * @property string|null $name Название
 * @property string|null $photo Фото
 */
class Theme extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%theme}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoryId', 'levelId'], 'integer'],
            [['name', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'categoryId' => Yii::t('app', 'Категория'),
            'levelId' => Yii::t('app', 'Уровень'),
            'name' => Yii::t('app', 'Название'),
            'photo' => Yii::t('app', 'Фото'),
        ];
    }
}
