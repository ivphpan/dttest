<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%theme_word}}".
 *
 * @property int $themeId Тема
 * @property int $wordId Слово
 */
class ThemeWord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%theme_word}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['themeId', 'wordId'], 'required'],
            [['themeId', 'wordId'], 'integer'],
            [['themeId', 'wordId'], 'unique', 'targetAttribute' => ['themeId', 'wordId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'themeId' => Yii::t('app', 'Тема'),
            'wordId' => Yii::t('app', 'Слово'),
        ];
    }
}
