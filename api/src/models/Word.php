<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%word}}".
 *
 * @property int $id
 * @property string|null $name Слово
 * @property string|null $translation Перевод
 * @property string|null $transcription Транскрипция
 * @property string|null $example Пример
 * @property string|null $sound Аудио-файл
 */
class Word extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%word}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'translation', 'transcription', 'example', 'sound'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Слово'),
            'translation' => Yii::t('app', 'Перевод'),
            'transcription' => Yii::t('app', 'Транскрипция'),
            'example' => Yii::t('app', 'Пример'),
            'sound' => Yii::t('app', 'Аудио-файл'),
        ];
    }
}
