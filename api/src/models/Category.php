<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string|null $name Название
 * @property string|null $icon Иконка
 */
class Category extends \yii\db\ActiveRecord
{
    const ICON_DIR = 'images/category-icon';
    /**
     * \yii\web\UploadedFile
     */
    public $iconFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'icon'], 'string', 'max' => 255],
            [['name', 'icon'], 'required'],
            ['iconFile', 'file', 'mimeTypes' => ['image/*'], 'skipOnEmpty' => false],
            ['iconFile', 'file', 'mimeTypes' => ['image/*'], 'skipOnEmpty' => true, 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'icon' => Yii::t('app', 'Иконка'),
        ];
    }

    public function afterValidate()
    {
        if ($this->iconFile instanceof \yii\web\UploadedFile) {
            $this->icon = self::ICON_DIR . '/' . \Yii::$app->security->generateRandomString(8) . '.' .  $this->iconFile->getExtension();
            $this->iconFile->saveAs($this->icon);
        }
    }
}
