<?php

namespace app\controllers\admin;

use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller
{
    public $layout = '@app/views/layouts/admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['index', 'create', 'update', 'delete'],
                    ]
                ]
            ]
        ];
    }
}
