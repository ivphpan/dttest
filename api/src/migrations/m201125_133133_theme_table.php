<?php

use yii\db\Migration;

/**
 * Class m201125_133133_theme_table
 */
class m201125_133133_theme_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{theme}}', [
            'id' => $this->primaryKey(),
            'categoryId' => $this->integer()->comment('Категория'),
            'levelId' => $this->integer()->comment('Уровень'),
            'name'=>$this->string()->comment('Название'),
            'photo'=>$this->string()->comment('Фото'),
        ]);

        $this->createIndex('ixCategoryLevel', '{{theme}}', [
            'categoryId',
            'levelId',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{theme}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_133133_theme_table cannot be reverted.\n";

        return false;
    }
    */
}
