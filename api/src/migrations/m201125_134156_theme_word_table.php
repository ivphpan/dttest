<?php

use yii\db\Migration;

/**
 * Class m201125_134156_theme_word_table
 */
class m201125_134156_theme_word_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{theme_word}}', [
            'themeId' => $this->integer()->comment('Тема'),
            'wordId' => $this->integer()->comment('Слово'),
        ]);

        $this->addPrimaryKey('themeWord', '{{theme_word}}', [
            'themeId',
            'wordId',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{theme_word}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_134156_theme_word_table cannot be reverted.\n";

        return false;
    }
    */
}
