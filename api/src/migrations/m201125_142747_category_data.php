<?php

use yii\db\Migration;

/**
 * Class m201125_142747_category_data
 */
class m201125_142747_category_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $categoryList = [
            [
                'name' => 'People and things',
                'icon' => 'images/category-icon/01.png',
            ],
            [
                'name' => 'Appearance and character',
                'icon' => 'images/category-icon/02.png',
            ],
            [
                'name' => 'Time and dates',
                'icon' => 'images/category-icon/03.png',
            ],
        ];

        foreach ($categoryList as $categoryItem) {
            $category = new \app\models\Category;
            $category->name = $categoryItem['name'];
            $category->icon = $categoryItem['icon'];
            $category->save(false);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->truncateTable('{{category}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_142747_category_data cannot be reverted.\n";

        return false;
    }
    */
}
