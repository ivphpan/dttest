<?php

use yii\db\Migration;

/**
 * Class m201125_133644_word_table
 */
class m201125_133644_word_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->createTable('{{word}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Слово'),
            'translation' => $this->string()->comment('Перевод'),
            'transcription' => $this->string()->comment('Транскрипция'),
            'example' => $this->string()->comment('Пример'),
            'sound' => $this->string()->comment('Аудио-файл'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{word}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_133644_word_table cannot be reverted.\n";

        return false;
    }
    */
}
