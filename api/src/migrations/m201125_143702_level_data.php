<?php

use yii\db\Migration;

/**
 * Class m201125_143702_level_data
 */
class m201125_143702_level_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $levelList = [
            [
                'name' => 'Pre-intermediate',
                'code' => 'a2',
            ],
            [
                'name' => 'Intermediate',
                'code' => 'b1',
            ],
            [
                'name' => 'Upper-ntermediate',
                'code' => 'b2',
            ]
        ];

        foreach ($levelList as $levelItem) {
            $level = new \app\models\Level;
            $level->name = $levelItem['name'];
            $level->code = $levelItem['code'];
            $level->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->truncateTable('{{level}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_143702_level_data cannot be reverted.\n";

        return false;
    }
    */
}
