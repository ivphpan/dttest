<?php

use yii\db\Migration;

/**
 * Class m201125_132809_level_table
 */
class m201125_132809_level_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{level}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'code' => $this->string()->comment('Код')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{level}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_132809_level_table cannot be reverted.\n";

        return false;
    }
    */
}
