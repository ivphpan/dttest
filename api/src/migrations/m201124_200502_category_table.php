<?php

use yii\db\Migration;

/**
 * Class m201124_200502_category_table
 */
class m201124_200502_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->createTable('{{category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'icon' => $this->string()->comment('Иконка'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{category}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_200502_category_table cannot be reverted.\n";

        return false;
    }
    */
}
