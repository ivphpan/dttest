<?php

use yii\db\Migration;

/**
 * Class m201125_150040_word_data
 */
class m201125_150040_word_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $wordList = [
            [
                'name' => 'Я должен идти',
                'translation' => 'I must go.',
                'transcription' => 'ай маст гоУ',
                'example' => 'I must go.',
                'sound' => ''
            ],
            [
                'name' => 'Очень жаль, что Вы уходите.',
                'translation' => 'I\'m so sorry to see you go.',
                'transcription' => 'ай эм с\'ори ту си ю гоу',
                'example' => 'I\'m so sorry to see you go.',
                'sound' => ''
            ],
            [
                'name' => 'Вы ведь не торопитесь?',
                'translation' => 'You\'re not in a hurry, are you?',
                'transcription' => 'ю ар нот ин э х\'эри \'ар ю',
                'example' => 'You\'re not in a hurry, are you?',
                'sound' => ''
            ],
            [
                'name' => 'Надеюсь, мы ещё встретимся.',
                'translation' => 'You\'re not in a hurry, are you?',
                'transcription' => 'ай х\'оУп Уил мит эг\'ейн',
                'example' => 'You\'re not in a hurry, are you?',
                'sound' => ''
            ],
            [
                'name' => 'Был рад Вас видеть.',
                'translation' => 'I\'ve enjoyed seeing you.',
                'transcription' => 'айв индж\'ёйд с\'ыинг ю',
                'example' => 'I\'ve enjoyed seeing you.',
                'sound' => ''
            ],
            [
                'name' => 'Я сейчас вернусь.',
                'translation' => 'I\'ll be right back.',
                'transcription' => 'айл би райт бэк',
                'example' => 'I\'ll be right back.',
                'sound' => ''
            ],
            [
                'name' => 'Я вам не мешаю?',
                'translation' => 'Am I bothering you?',
                'transcription' => 'эм ай б\'аЗэринг ю',
                'example' => 'Am I bothering you?',
                'sound' => ''
            ],
        ];

        foreach ($wordList as $wordItem) {
            $word = new \app\models\Word;
            $word->name = $wordItem['name'];
            $word->translation = $wordItem['translation'];
            $word->transcription = $wordItem['transcription'];
            $word->example = $wordItem['example'];
            $word->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->truncateTable('{{word}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_150040_word_data cannot be reverted.\n";

        return false;
    }
    */
}
